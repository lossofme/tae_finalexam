#include <ignition/math/Pose3.hh>
#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

namespace gazebo
{
class Factory : public WorldPlugin
{
  public: void Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/)
  {
    // Option 1: Insert model from file via function call.
    // The filename must be in the GAZEBO_MODEL_PATH environment variable.

    // Option 3: Insert model from file via message passing.
    {
      // Create a new transport node
      transport::NodePtr node(new transport::Node());

      // Initialize the node with the world name
      node->Init(_parent->GetName());

      // Create a publisher on the ~/factory topic
      transport::PublisherPtr factoryPub =
      node->Advertise<msgs::Factory>("~/factory");

      // Create the message
      msgs::Factory msg,msg1,msg2;

      // Model file to load
      msg.set_sdf_filename("model://grey_wall");

      // Pose to initialize the model to
      msgs::Set(msg.mutable_pose(),
          ignition::math::Pose3d(
            ignition::math::Vector3d(4, 0, 0),
            ignition::math::Quaterniond(0, 0, 1.57)));

      // Model file to load
      msg1.set_sdf_filename("model://box");

      // Pose to initialize the model to
      msgs::Set(msg1.mutable_pose(),
          ignition::math::Pose3d(
            ignition::math::Vector3d(3.3, 3, 0),
            ignition::math::Quaterniond(0, 0, 0)));

      // Model file to load
      msg2.set_sdf_filename("model://cylinder");

      // Pose to initialize the model to
      msgs::Set(msg2.mutable_pose(),
          ignition::math::Pose3d(
            ignition::math::Vector3d(1, -2, 0),
            ignition::math::Quaterniond(0, 0, 0)));

      // Send the message
      factoryPub->Publish(msg);
      factoryPub->Publish(msg1);
      factoryPub->Publish(msg2);
    }
    

  }
};

// Register this plugin with the simulator
GZ_REGISTER_WORLD_PLUGIN(Factory)
}
