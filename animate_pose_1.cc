/*
 * Copyright (C) 2012-2015 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include <ignition/math/Vector3.hh>
#include <ignition/math/Quaternion.hh>

#include "gazebo/common/CommonTypes.hh"
#include "gazebo/common/Animation.hh"
#include "gazebo/common/KeyFrame.hh"
#include "gazebo/physics/Model.hh"
#include "gazebo/gazebo.hh"

namespace gazebo
{
  class AnimatePose : public ModelPlugin
  {
    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
    {
      gazebo::common::PoseAnimationPtr anim5(
          new gazebo::common::PoseAnimation("test", 15.0, false));

      gazebo::common::PoseKeyFrame *key5;

      key5 = anim5->CreateKeyFrame(0);
      key5->Translation(ignition::math::Vector3d(0, -3, 0));
      key5->Rotation(ignition::math::Quaterniond(0, 0, 1.5707));

      key5 = anim5->CreateKeyFrame(5);
      key5->Translation(ignition::math::Vector3d(0, 0, 0));
      key5->Rotation(ignition::math::Quaterniond(0, 0, 1.5707));

      key5 = anim5->CreateKeyFrame(10);
      key5->Translation(ignition::math::Vector3d(0, 0, 0));
      key5->Rotation(ignition::math::Quaterniond(0, 0, 3.14));

      key5 = anim5->CreateKeyFrame(15);
      key5->Translation(ignition::math::Vector3d(-1.5, 0, 0));
      key5->Rotation(ignition::math::Quaterniond(0, 0, 3.14));
      
      

      _parent->SetAnimation(anim5);
    }
  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(AnimatePose)
}